﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kostki
{
    class Program
    {
        unsafe static void Rekurencja(int n, int[] single, int[] all, int[] fAll,
            int wartosc, int indeksObecny, int *indeksOstatniego)
        {
            n--;
            if (n == 0)
            {
                bool check = false;
                for (int j = 0; j < all.Length; j++)
                {
                    if (all[j] == wartosc)
                    {
                        fAll[j] += 1;
                        check = true;
                        break;
                    }
                }
                if (check == false)
                {
                    all[*indeksOstatniego] = wartosc;
                    fAll[*indeksOstatniego] += 1;
                    *indeksOstatniego += 1;

                }
                return;
            }
            for (int i = 0; i < single.Length; i++)
            {
                Rekurencja(n, single, all, fAll, wartosc+single[i],i, indeksOstatniego);
            }
        }
        static unsafe void Main(string[] args)
        {
            int n; // ilosc kostek
            int k; //ile mozliwosci ma kostka
            double omega; // liczebnosc przestrzeni zdarzen
            int[] single; //mozliwosci w pojedynczej kostce
            int[] all;// tablica liczb ktore mozna uzyskac
            int[] fAll;//tablica dopasowywujaca kazdej z powyższych możliwości ilosc wywolan
            int indeksOstatniego = 0;
            string input;

            Console.WriteLine("Podaj liczbę kostek");
            input = Console.ReadLine();
            n = Int32.Parse(input);
            Console.WriteLine("Podaj ilość możliwości jaką każda kostka posiada");
            input = Console.ReadLine();
            k = Int32.Parse(input);
            omega = Math.Pow((double)k, (double)n);
            single = new int[k];

             if (n >= 1)
            {
                all = new int[n * k];
                fAll = new int[n * k];
            }
            else
            {
                Console.WriteLine("Podaj odpowiednią liczbę kostek");
                return;
            }
            for (int i = 0; i < all.Length; i++)
            {
                all[i] = -1;
                fAll[i] = 0;
            }
            Console.WriteLine("Podaj po kolei każdą możliwość jaką kostka posiada");
            for (int i=0;i<k;i++)
            {
                input = Console.ReadLine();
                single[i] = Int32.Parse(input);
            }
            //liczenie
            for (int i = 0; i < k; i++)
            {
                Rekurencja(n, single, all, fAll, single[i], i, &indeksOstatniego);
            }
            Console.WriteLine("Prawdopodobieństwo podczas rzutu wszystkimi kostkami na raz to:");
            double tmp;
            for (int i = 0; i < all.Length; i++)
            {
                tmp = fAll[i] / omega;
                Console.WriteLine("Dla wartości {0} prawdopodobieństwo jej otrzymania wynosi {1}", all[i], tmp);
            }
            Console.WriteLine("Ilość elementów przestrzeni zdarzeń to {0}", omega);
            Console.ReadKey();
        }
    }
}
